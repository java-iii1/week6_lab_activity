/**
 * Vector3dtests is a class used to test the java file called vector3d.java
 * @author Danny hoang
 * @version 10/02/2023
 */


package linearalgebra;

import static org.junit.Assert.*;
import org.junit.Test;

public class Vector3dTests {
    @Test
    public void testGetter() {
        final double tolerance = .0001;
        Vector3d testVector3d = new Vector3d(1, 2, 3);
        assertEquals(testVector3d.getX(), 1, tolerance);
        assertEquals(testVector3d.getY(), 2, tolerance);
        assertEquals(testVector3d.getZ(), 3, tolerance);

    }

    @Test
    public void testMagnitude() {
        final double tolerance = .0001;
        Vector3d testVector3d = new Vector3d(1, 2, 3);
        assertEquals(testVector3d.magnitude(), 3.74165738773941, tolerance);
    }

    @Test
    public void testdotProduct() {
        final double tolerance = .0001;
        Vector3d testVector3d = new Vector3d(1, 1, 2);
        Vector3d testVector3dtwo = new Vector3d(2, 3, 4);

        assertEquals(13, testVector3d.dotProduct(testVector3dtwo), tolerance);
    }

    @Test
    public void testAdd() {
        final double tolerance = .0001;
        Vector3d testVector3d = new Vector3d(1, 1, 2);
        Vector3d testVector3dtwo = new Vector3d(2, 3, 4);
        Vector3d testVector3dThree = testVector3d.add(testVector3dtwo);
        assertEquals(3, testVector3dThree.getX(), tolerance);
        assertEquals(4, testVector3dThree.getY(), tolerance);
        assertEquals(6, testVector3dThree.getZ(), tolerance);

    }
}

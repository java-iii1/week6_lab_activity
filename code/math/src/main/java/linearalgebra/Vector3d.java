/**
 * Vector3d is a class used to create vectors and do various of methods towards one or two vectors.
 * @author Danny hoang
 * @version 10/02/2023
 */
package linearalgebra;

public class Vector3d {

    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    /**
     * returns x of vector
     * @return the value of x
     */
    public double getX() {
        return this.x;
    }
    /**
     * returns y of vector
     * @return the value of y
     */
    public double getY() {
        return this.y;
    }
    /**
     * returns z of vector
     * @return the value of z
     */
    public double getZ() {
        return this.z;
    }
    /**
     * returns x of vector
     * @return the value where vector's x power to the 2 + y power to the 2 + z power to the 2
     */
    public double magnitude() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
    }
     /**
     * @param Vector3d the vector used to be multipled by current vector.
     * @return the value where vector's x multipled by inputed vector's x
     */
    public double dotProduct(Vector3d secondVector) {
        return (secondVector.x * this.x) + (secondVector.y * this.y) + (secondVector.z * this.z);
    }
     /**
     * @param Vector3d the vector used to be added by current vector.
     * @return the value where vector's x,y,z added by inputed vector's x,y,z
     */
    public Vector3d add(Vector3d secondVector) {

        Vector3d newV3d = new Vector3d(secondVector.x + this.x, secondVector.y + this.y, secondVector.z + this.z);
        return newV3d;
    }

}
